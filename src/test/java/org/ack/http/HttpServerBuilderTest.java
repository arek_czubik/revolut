package org.ack.http;

import io.restassured.RestAssured;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class HttpServerBuilderTest {

    static int port = 8112;
    HttpServerExtended server = null;

    @BeforeClass
    public static void setupSpec() {
        RestAssured.port = port;
    }

    @After
    public void cleanup() {
        if (server != null) {
            server.stop(0);
        }
    }

    @Test
    public void whenServerIsBuildWithOneGetHandler_thenOneVerbRouterWithOneHandlerIsRegisterd() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.GET, (exchange, r) -> null)
                .build(port);
        server.start();

        // expect
        assertThat(server.getMethodRouters().size(), is(1));
        assertThat(server.getMethodRouters().get(0).getPath(), is("/test"));
        assertThat(server.getMethodRouters().get(0).getHandlers().size(), is(1));
        assertTrue(server.getMethodRouters().get(0).getHandlers().stream().collect(toList()).get(0).getRequestMatcher().matches("/test", HttpVerb.GET));
    }

    @Test
    public void whenServerIsBuildWIthHandlersForTwoDifferentPaths_thenTwoVerbRoutersAreCreated() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test1", HttpVerb.GET, (exchange, r) -> null )
                .withMapping("/test2", HttpVerb.GET, (exchange, r) -> null )
                .build(port);
        server.start();

        // expect
        assertThat(server.getMethodRouters().size(), is(2));
    }

    @Test
    public void whenServerIsBuildWithHandlerForOnePathWithTwoVerbs_thenOneRouterWithTwoHandlersIsCreated() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test1", HttpVerb.GET, (exchange,r ) -> null )
                .withMapping("/test1", HttpVerb.POST, (exchange,r ) -> null )
                .build(port);
        server.start();

        // expect
        assertThat(server.getMethodRouters().size(), is(1));
        assertThat(server.getMethodRouters().get(0).getHandlers().size(), is(2));
        assertTrue(server.getMethodRouters().get(0).getHandlers().stream().collect(toList()).get(0).getRequestMatcher().matches("/test1", HttpVerb.GET));
        assertTrue(server.getMethodRouters().get(0).getHandlers().stream().collect(toList()).get(1).getRequestMatcher().matches("/test1", HttpVerb.POST));
    }

    @Test
    public void whenServerIsBuildWIthTwoHandlersForTheSamePathSameVerbDifferentArguemnts_thenCorrentMatchersAreBuild() throws IOException {
        // given
        server = new HttpServerBuilder()
                .withMapping("/test1", HttpVerb.GET, (exchange,r ) -> null)
                .withMapping("/test1", "/.*", HttpVerb.GET, (exchange,r ) -> null)
                .build(port);
        server.start();

        expect:
        assertThat(server.getMethodRouters().size(), is(1));
        assertThat(server.getMethodRouters().get(0).getHandlers().size(), is(2));
        assertTrue(server.getMethodRouters().get(0).getHandlers().stream().collect(toList()).get(0).getRequestMatcher().matches("/test1", HttpVerb.GET));
        assertTrue(server.getMethodRouters().get(0).getHandlers().stream().collect(toList()).get(1).getRequestMatcher().matches("/test1/someArgument", HttpVerb.GET));

    }



}