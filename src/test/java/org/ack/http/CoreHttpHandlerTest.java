package org.ack.http;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import org.ack.http.exception.BadRequestException;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.kohsuke.rngom.util.Uri;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;

/**
 * Tests contract of CoreHttpHandler
 *
 * when returned
 * - null                   -> onSuccess(HttpExchange )
 * - HttpSuccessResponse    -> onSuccess(HttpExchange , HttpSuccessResponse )
 * - HttpFailureResponse    -> onFailure(HttpExchange , HttpFailureResponse )
 * - Object                 -> onSuccess(HttpExchange , Object )
 *
 * when thrown
 * - Exception              -> onException(HttpExchange , Exception )
 * - BadRequestException    -> onException(HttpExchange , BadRequestException )
 */
public class CoreHttpHandlerTest {


    HttpExchange mockExchange() {
        var exchange = Mockito.mock(HttpExchange.class);
        var httpContext = Mockito.mock(HttpContext.class);
        var headers = new Headers();

        Mockito.when(httpContext.getPath()).thenReturn("/test");
        Mockito.when(exchange.getRequestMethod()).thenReturn("GET");
        URI uri = null;
        try {
            uri = new URI("/test");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Mockito.when(exchange.getRequestURI()).thenReturn(uri);
        Mockito.when(exchange.getHttpContext()).thenReturn(httpContext);
        Mockito.when(exchange.getRequestHeaders()).thenReturn(headers);

        return exchange;
    }

    CoreHttpHandler createHandler(RequestHandler handler) {
        return new CoreHttpHandler(
                new RequestMatcher("/test", HttpVerb.GET),
                handler,
                successHandler,
                failureHandler,
                requestReader,
                responseWriter
        );
    }

    ResponseSuccessHandler successHandler = null;
    ResponseFailureHandler failureHandler = null;
    HttpExchange exchange = null;
    RequestReader requestReader = null;
    ResponseWriter responseWriter = null;

    @Before
    public void setup() {
        successHandler = Mockito.mock(ResponseSuccessHandler.class);
        failureHandler = Mockito.mock(ResponseFailureHandler.class);
        exchange = mockExchange();
        requestReader = Mockito.mock(RequestReader.class);
        responseWriter = Mockito.mock(ResponseWriter.class);
    }

    @Test
    public void whenResultIsNull_thenOnSuccessIsCalled() throws IOException {
        // given

        var handler = createHandler( (ex, r) -> null );

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(successHandler, Mockito.times(1)).onSuccess(exchange);
    }

    @Test
    public void whenResultIsHttpSuccessResponse_thenOnSuccessIsCalled() throws IOException {
        // given
        var response = new HttpSuccessResponse<Integer>(HttpStatus.SC_CREATED, 1L, 25);
        var handler = createHandler(  (ex, r) -> response );

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(successHandler,
                Mockito.times(1)).onSuccess(eq(exchange), eq(HttpStatus.SC_CREATED), eq(response.getData())
        );
    }

    @Test
    public void whenResultIsHttpFailureResponse_thenOnFailureIsCalled() throws IOException {
        // given
        var response = new HttpFailureResponse(407, "test message", 1L);
        var handler = createHandler( (ex, r) -> response);

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(failureHandler, Mockito.times(1)).onFailure(eq(exchange), eq(response));
    }

    @Test
    public void whenResultIsAnyObject_thanOnSuccessIsCalled() throws IOException {
        // given
        var response = Integer.valueOf(10);
        var handler = createHandler( (ex, r) -> response);

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(successHandler, Mockito.times(1)).onSuccess(eq(exchange), eq(HttpStatus.SC_OK), eq(response));
    }

    @Test
    public void whenResultIsAnException_thenOnExceptionIsCalled() throws IOException {
        // given
        var exception = new RuntimeException("failed");
        var handler = createHandler( (ex, r) -> {throw exception; });

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(failureHandler, Mockito.times(1)).onException(eq(exchange), eq(exception));
    }

    @Test
    public void whenResultIsABadRequestException_thenOnExceptionIsCalled() throws IOException {
        // given
        var exception = new BadRequestException("failed");
        var handler = createHandler( (ex, r) -> { throw exception; } );

        // when
        handler.handle(exchange);

        // then
        Mockito.verify(failureHandler, Mockito.times(1)).onException(eq(exchange), eq(exception));
    }

}