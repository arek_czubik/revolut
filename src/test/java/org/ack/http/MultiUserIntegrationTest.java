package org.ack.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.Money;
import org.ack.domain.accounts.business.impl.AccountsServiceImpl;
import org.ack.domain.accounts.business.impl.InMemoryAccountsRepository;
import org.ack.domain.accounts.business.impl.TransfersServiceImpl;
import org.ack.domain.accounts.interfaces.rest.AccountsResourceRequestHandlers;
import org.ack.domain.accounts.interfaces.rest.TransferDto;
import org.ack.domain.accounts.interfaces.rest.TransfersResourceRequestHandler;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Simulates n parallel users doing m requests each. Checks if all requests finished with success.
 */
public class MultiUserIntegrationTest {

    Account account1 = new Account(AccountId.createRandom(), Money.of(100000));
    Account account2 = new Account(AccountId.createRandom(), Money.of(100000));
    Account account3 = new Account(AccountId.createRandom(), Money.of(100000));
    ObjectMapper mapper = new ObjectMapper();

    static class ExecutionResult {
        private int statusCode;
        private long time;

        public ExecutionResult(int statusCode, long time) {
            this.statusCode = statusCode;
            this.time = time;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public long getTime() {
            return time;
        }

        @Override
        public String toString() {
            return "statusCode=" + statusCode + ", time=" + time;
        }
    }


    @Test
    public void testAccessingAccountsResourceByManyUsersAtTheSameTime() throws IOException {


        var inMemoryAccountsRepository = new InMemoryAccountsRepository();
        inMemoryAccountsRepository.add(account1);
        inMemoryAccountsRepository.add(account2);
        inMemoryAccountsRepository.add(account3);
        var accountsService = new AccountsServiceImpl(inMemoryAccountsRepository);
        var accountsResourceRequestHandlers = new AccountsResourceRequestHandlers(accountsService);

        var transfersService = new TransfersServiceImpl(accountsService);
        var transfersResourceRequestHandler = new TransfersResourceRequestHandler(transfersService);

        HttpServerExtended server = new HttpServerBuilder()
                .withMapping("/api/v1/accounts", "/.+", HttpVerb.GET, accountsResourceRequestHandlers::handleGetAccount)
                .withMapping("/api/v1/accounts", HttpVerb.GET, accountsResourceRequestHandlers::handleGetAccounts)
                .withMapping("/api/v1/accounts", HttpVerb.POST, accountsResourceRequestHandlers::handlePostAccount)
                .withMapping("/api/v1/transfers", HttpVerb.POST, transfersResourceRequestHandler::handlePostTransfer)
                .build(8111);

        server.start();

        RestAssured.port = 8111;

        ExecutorService executor = Executors.newFixedThreadPool(4);
        CompletionService<Collection<ExecutionResult>> completionService = new ExecutorCompletionService<>(executor);

        // warm up server
        when().get("/api/v1/accounts/" + account1.getAccountId().toString());

        int usersCount = 10;
        int requestsCount = 100;

        performRequests(completionService, usersCount, requestsCount, () -> when().get("/api/v1/accounts/" + account1.getAccountId().toString()).statusCode()
        );

        performRequests(completionService, usersCount, requestsCount, () -> {
                    TransferDto transferDto = new TransferDto(account1.getAccountId().toString(), account2.getAccountId().toString(), "1");

                    try {
                        byte[] bytes = mapper.writeValueAsBytes(transferDto);

                        Response response =
                                given()
                                        .contentType(ContentType.JSON)
                                        .accept(ContentType.JSON)
                                        .body(bytes)
                                        .post("/api/v1/transfers");

                        return response.getStatusCode();

                    } catch (JsonProcessingException e) {
                        return 0;
                    }
                }
        );

        List<ExecutionResult> results = new LinkedList<>();
        for (int i = 0; i < 2 * usersCount; ++i) {
            try {
                results.addAll(completionService.take().get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        server.stop(0);

        assertThat(
                results.stream().filter(result -> result.statusCode == 200).collect(toList()).size(),
                is(2 * (usersCount * requestsCount)));

//        results
//                .stream()
//                //.sorted(Comparator.comparingLong(ExecutionResult::getTime))
//                .forEach( result -> System.out.println(result));
    }

    private void performRequests(CompletionService<Collection<ExecutionResult>> service, int usersCount, int requestCount, Supplier<Integer> request) {
        for (int user = 0; user < usersCount; ++user) {
            service.submit(() -> {

                List<ExecutionResult> results = new ArrayList();

                for (int i = 0; i < requestCount; ++i) {
                    wait(10);

                    long begin = System.currentTimeMillis();
                    Integer statusCode = request.get();
                    long elapsed = System.currentTimeMillis() - begin;
                    results.add(new ExecutionResult(statusCode, elapsed));
                }

                return results;
            });
        }
        ;
    }

    private void wait(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
