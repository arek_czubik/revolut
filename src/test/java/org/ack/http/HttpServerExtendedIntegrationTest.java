package org.ack.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.ack.http.exception.BadRequestException;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class HttpServerExtendedIntegrationTest {

    static int port = 8112;
    HttpServerExtended server = null;

    @BeforeClass
    public static void setupSpec() {
        RestAssured.port = port;
    }

    @After
    public void cleanup() {
        if (server != null) {
            server.stop(0);
        }
    }

    @Test
    public void whenHandlerResultingInSuccess_then200IsReturned() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.GET, (exchange, r) -> null)
                .build(port);
        server.start();

        // when
        var response = RestAssured.when().get("/test").then();

        // then
        response.statusCode(200);
    }

    @Test
    public void whenHandlerResultingInBadRequestException_then400IsReturned() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.GET, (exchange, r) -> { throw new BadRequestException("terefere"); })
                .build(port);
        server.start();

        // when
        var response = RestAssured.when().get("/test").then();

        // then
        response.statusCode(400);
    }

    @Test
    public void whenNoHandlerIsRegisteredForGivenPath_thenStatusIs404NotFound() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.GET, (exchange, r) -> {throw new BadRequestException("terefere"); })
                .build(port);
        server.start();

        // when
        var response = RestAssured.when().get("/other").then();

        // then
        response.statusCode(404);
    }

    @Test
    public void whenNoHandlerIsRegisteredForGivenVerb_then404NotFoundIsReturned() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.GET, (exchange, r) -> {throw new BadRequestException("terefere"); })
                .build(port);
        server.start();

        // when
        var response = RestAssured.when().post("/test").then();

        // then
        response.statusCode(404);
    }

    @Test
    public void whenPostedContentTypeIsNotSupported_then415UnsupportedMediaTypeIsReturned() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.POST, (exchange, r) -> null)
                .build(port);
        server.start();

        // when
        var response =
                given()
                        .contentType("application/xml")
                        .post("/test")
                        .then();

        // then
        response.statusCode(415);
    }

    @Test
    public void whenRequestedResponseTypeIsNotSuppored_then406NotAcceptableIsReturned() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.POST, (exchange, r) -> null)
                .build(port);
        server.start();

        // when
        var response =
                given()
                        .accept(ContentType.XML)
                        .contentType(ContentType.JSON)
                        .post("/test")
                        .then();

        // then
        response.statusCode(406);
    }

    @Test
    public void whenHandlerFailsToGetBody_then500IsReturnedWithProperMessage() throws IOException {

        // given
        server = new HttpServerBuilder()
                .withMapping("/test", HttpVerb.POST, (exchange, r) -> r.readBodyAs(exchange, Integer.class))
                .build(port);
        server.start();

        var mapper = new ObjectMapper();

        // when
        var response =
                given()
                        .accept(ContentType.JSON)
                        .contentType(ContentType.JSON)
                        .body(mapper.writeValueAsBytes(new byte[] { 1, 2, 3}))
                        .post("/test");

        // then
        response
                .then()
                .assertThat()
                .statusCode(400)
                .body("statusCode", Matchers.is(400))
                .body("message", Matchers.is("Bad request. Cause: Failed to read body. Cause: Failed to deserialize type java.lang.Integer"));
    }

}
