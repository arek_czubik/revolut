package org.ack.domain.accounts.business.impl;

import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransfersServiceImplTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void whenBothAccountExistAndSourceHasEnoughMoney_thenMoneyTransferIsExecutedForSourceAccount() {
        // given
        var account1 = spy(new Account(AccountId.createRandom(), Money.of(10)));
        var account2 = new Account(AccountId.createRandom(), Money.of(0));
        var accountService = mock(AccountsService.class);
        when(accountService.findById(eq(account1.getAccountId()))).thenReturn(Optional.of(account1));
        when(accountService.findById(eq(account2.getAccountId()))).thenReturn(Optional.of(account2));
        var transfersService = new TransfersServiceImpl(accountService);

        // when
        transfersService.transfer(account1.getAccountId(), account2.getAccountId(), Money.of(5));

        // then
        verify(account1, times(1)).transferMoneyTo(eq(account2), eq(Money.of(5)));
    }

    @Test
    public void whenBothAccountsExistAndSourceDoesNotHaveEnoughMoney_thenExceptionIsThrown() {
        // given
        var account1 = spy(new Account(AccountId.createRandom(), Money.of(10)));
        var account2 = new Account(AccountId.createRandom(), Money.of(0));
        var accountService = mock(AccountsService.class);
        when(accountService.findById(eq(account1.getAccountId()))).thenReturn(Optional.of(account1));
        when(accountService.findById(eq(account2.getAccountId()))).thenReturn(Optional.of(account2));
        var transfersService = new TransfersServiceImpl(accountService);

        // then
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage(String.format("Account with id=%s has not enough money to transfer 15", account1.getAccountId()));

        // when
        transfersService.transfer(account1.getAccountId(), account2.getAccountId(), Money.of(15));
    }

    @Test
    public void whenAnyAccountIsMissing_thenAccountNotFoundExceptionIsThrown() {
        // given
        var account1 = spy(new Account(AccountId.createRandom(), Money.of(10)));
        var accountService = mock(AccountsService.class);
        var account2Id = AccountId.createRandom();
        when(accountService.findById(eq(account1.getAccountId()))).thenReturn(Optional.of(account1));
        when(accountService.findById(eq(account2Id))).thenReturn(Optional.empty());
        var transfersService = new TransfersServiceImpl(accountService);

        // then
        exceptionRule.expect(AccountNotFoundException.class);
        exceptionRule.expectMessage(String.format("Account with id %s does not exist", account2Id));

        // when
        transfersService.transfer(account1.getAccountId(), account2Id, Money.of(15));
    }

}