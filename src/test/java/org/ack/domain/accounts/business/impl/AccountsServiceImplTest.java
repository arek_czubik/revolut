package org.ack.domain.accounts.business.impl;

import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsRepository;
import org.ack.domain.accounts.business.api.Money;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.is;

public class AccountsServiceImplTest {

    @Test
    public void whenFindByIdIsCalledForAccountThatDoesNotExist_thenEmptyOptionalIsReturned() {
        // given
        var repository = mock(AccountsRepository.class);
        when(repository.findById(any(AccountId.class))).thenReturn(Optional.empty());
        var service = new AccountsServiceImpl(repository);

        // then
        var account = service.findById(AccountId.createRandom());


        // then
        assertThat(account, is(Optional.empty()));
    }

    @Test
    public void whenFindByIdIsCalledForAccountThatExists_thenAccountIsReturned() {
        // given
        var repository = mock(AccountsRepository.class);
        var account = new Account(AccountId.createRandom(), Money.of(0));
        when(repository.findById(eq(account.getAccountId()))).thenReturn(Optional.of(account));
        var service = new AccountsServiceImpl(repository);

        // when
        var found = service.findById(account.getAccountId());

        // then
        assertThat(found, is(Optional.of(account)));
    }

    @Test
    public void whenAddNewAccountIsCalled_thenNewAccountIsCreatedPutInRepositoryAndReturned() {
        // given
        var repository = mock(AccountsRepository.class);
        var service = new AccountsServiceImpl(repository);

        // when
        var account = service.addNewAccount(Money.of(10));

        // then
        assertThat(account.getBalance(), is(Money.of(10)));
        verify(repository, times(1)).add(any(Account.class));
    }

    @Test
    public void whenGetAllIsCalled_thenAllAccountsAreReturned() {
        // given
        var account1 = new Account(AccountId.createRandom(), Money.of(10));
        var account2 = new Account(AccountId.createRandom(), Money.of(11));
        var account3 = new Account(AccountId.createRandom(), Money.of(12));
        var repository = mock(AccountsRepository.class);
        when(repository.getAll()).thenReturn(Arrays.asList(account1, account2, account3));
        var service = new AccountsServiceImpl(repository);

        // when
        var allAccounts = service.getAll();

        // then
        assertThat(allAccounts.size(), is(3));
    }

}