package org.ack.domain.accounts.business.api;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AccountTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void whenMoneyTransferredAreGreaterThanOnAnAccount_thenExceptionIsThrown() {
        // given
        var account1 = new Account(AccountId.createRandom(), Money.of(5));
        var account2 = new Account(AccountId.createRandom(), Money.of(0));

        // then
        exceptionRule.expect(IllegalStateException.class);
        exceptionRule.expectMessage("Account balance cannot be smaller than 0");

        // when
        account1.transferMoneyTo(account2, Money.of(10));
    }

    @Test
    public void whenTransferringMoneyThatAccountContains_thenBothAccountsAreUpdated() {
        // given
        var account1 = new Account(AccountId.createRandom(), Money.of(15));
        var account2 = new Account(AccountId.createRandom(), Money.of(0));

        // when
        account1.transferMoneyTo(account2, Money.of(10));

        // then
        assertThat(account1.getBalance(), is(Money.of(5)));
        assertThat(account2.getBalance(), is(Money.of(10)));
    }


}