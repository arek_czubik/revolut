package org.ack.domain.accounts.interfaces.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsRepository;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.ack.domain.accounts.business.impl.AccountsServiceImpl;
import org.ack.domain.accounts.business.impl.InMemoryAccountsRepository;
import org.ack.domain.accounts.business.impl.TransfersServiceImpl;
import org.ack.http.HttpServerBuilder;
import org.ack.http.HttpServerExtended;
import org.ack.http.HttpVerb;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test cases for POST /api/v1/transfers
 * <p>
 * 1. both accounts exist                           -> 200 money transferred
 * 2. destination account does not exist            -> 404 source account not changed
 * 3. source has not enough money                   -> 500 accounts not changed
 */
public class TransfersResourceRequestHandlerTest {

    @BeforeClass
    public static void beforeAll() {
        RestAssured.port = SERVER_PORT;
    }

    @After
    public void teardown() {
        if (server != null) {
            server.stop(0);
            server = null;
        }
    }

    HttpServerExtended server;
    private static int SERVER_PORT = 8011;

    @Test
    public void whenTransferBetweenExistingAccountWithValidBalance_200IsReturnedAndAccountsAreUpdated() throws IOException {
        // given
        Account account1 = new Account(AccountId.createRandom(), Money.of(10));
        Account account2 = new Account(AccountId.createRandom(), Money.of(5));
        InMemoryAccountsRepository accountsRepository = new InMemoryAccountsRepository();
        accountsRepository.add(account1);
        accountsRepository.add(account2);

        server = startServer(accountsRepository);
        server.start();

        ObjectMapper mapper = new ObjectMapper();
        TransferDto transferDto = new TransferDto(account1.getAccountId().toString(), account2.getAccountId().toString(), "4");

        // when
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(mapper.writeValueAsBytes(transferDto))
                .post("/api/v1/transfers");

        // then
        response.then()
                .statusCode(200);

        assertThat(account1.getBalance(), is(Money.of(6)));
        assertThat(account2.getBalance(), is(Money.of(9)));
    }

    @Test
    public void whenTransferWithNotExistingAccount_404IsReturnedAndAccountsAreNotUpdated() throws IOException {
        // given
        Account account1 = new Account(AccountId.createRandom(), Money.of(10));
        InMemoryAccountsRepository accountsRepository = new InMemoryAccountsRepository();
        accountsRepository.add(account1);

        server = startServer(accountsRepository);
        server.start();

        ObjectMapper mapper = new ObjectMapper();
        TransferDto transferDto =
                new TransferDto(
                        account1.getAccountId().toString(),
                        UUID.randomUUID().toString(),
                        "4"
                );

        // when
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(mapper.writeValueAsBytes(transferDto))
                .post("/api/v1/transfers");

        // then
        response.then()
                .statusCode(404);

        assertThat(account1.getBalance(), is(Money.of(10)));
    }

    @Test
    public void whenTransferWithNotEnoughMoney_500IsReturnedAndAccountsAreNotUpdated() throws IOException {
        // given
        Account account1 = new Account(AccountId.createRandom(), Money.of(10));
        Account account2 = new Account(AccountId.createRandom(), Money.of(10));
        InMemoryAccountsRepository accountsRepository = new InMemoryAccountsRepository();
        accountsRepository.add(account1);
        accountsRepository.add(account2);

        server = startServer(accountsRepository);
        server.start();

        ObjectMapper mapper = new ObjectMapper();
        TransferDto transferDto =
                new TransferDto(
                        account1.getAccountId().toString(),
                        account2.getAccountId().toString(),
                        "40"
                );

        // when
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(mapper.writeValueAsBytes(transferDto))
                .post("/api/v1/transfers");

        // then
        response.then()
                .statusCode(500);

        assertThat(account1.getBalance(), is(Money.of(10)));
    }

    private HttpServerExtended startServer(AccountsRepository accountsRepository) throws IOException {
        AccountsService accountsService = new AccountsServiceImpl(accountsRepository);
        AccountsResourceRequestHandlers accountsHandlers = new AccountsResourceRequestHandlers(accountsService);

        TransfersServiceImpl transfersService = new TransfersServiceImpl(accountsService);
        TransfersResourceRequestHandler transfersHandlers = new TransfersResourceRequestHandler(transfersService);

        return
                new HttpServerBuilder()
                        .withMapping("/api/v1/accounts", "/.*", HttpVerb.GET, accountsHandlers::handleGetAccount)
                        .withMapping("/api/v1/accounts", HttpVerb.GET, accountsHandlers::handleGetAccounts)
                        .withMapping("/api/v1/accounts", HttpVerb.POST, accountsHandlers::handlePostAccount)
                        .withMapping("/api/v1/transfers", HttpVerb.POST, transfersHandlers::handlePostTransfer)
                        .build(SERVER_PORT);
    }

}
