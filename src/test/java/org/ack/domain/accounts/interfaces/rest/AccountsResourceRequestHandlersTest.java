package org.ack.domain.accounts.interfaces.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsRepository;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.ack.domain.accounts.business.impl.AccountsServiceImpl;
import org.ack.domain.accounts.business.impl.InMemoryAccountsRepository;
import org.ack.domain.accounts.business.impl.TransfersServiceImpl;
import org.ack.http.HttpServerBuilder;
import org.ack.http.HttpServerExtended;
import org.ack.http.HttpVerb;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases
 *
 * - POST /api/v1/accounts                          -> 201 creates account
 * - GET /api/v1/accounts                           -> 200 with list of accounts
 * - GET /api/v1/accounts/{existing account id}     -> 200 with account info
 * - GET /api/v1/accounts/{not existing account id} -> 404 account not found with message in response
 */
public class AccountsResourceRequestHandlersTest {

    @BeforeClass
    public static void beforeAll() {
        RestAssured.port = 8011;
    }

    HttpServerExtended server;

    @After
    public void teardown() {
        if (server != null) {
            server.stop(0);
            server = null;
        }
    }

    AccountsService accountsService = null;

    @Test
    public void whenGetAccountsCalled_thenAllAccountsInRepositoryAreReturned() throws IOException {
        // given
        var accountsRepository = new InMemoryAccountsRepository();
        var accountId1 = AccountId.of(UUID.randomUUID().toString());
        var accountId2 = AccountId.of(UUID.randomUUID().toString());
        var accountId3 = AccountId.of(UUID.randomUUID().toString());
        accountsRepository.add(new Account(accountId1, Money.of(10)));
        accountsRepository.add(new Account(accountId2, Money.of(9)));
        accountsRepository.add(new Account(accountId3, Money.of(8)));

        server = startServer(accountsRepository);
        server.start();

        ObjectMapper mapper = new ObjectMapper();

        // when
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(mapper.writeValueAsBytes(new AccountDto(null, new BigDecimal(10))))
                .post("/api/v1/accounts");

        // then
        response
                .then()
                .statusCode(201)
                .body("balance", is(10));

    }

    @Test
    public void whenPostAccountsCalled_thenNewAccountIsAddedToRepository() throws IOException {
        // given
        var accountsRepository = new InMemoryAccountsRepository();

        server = startServer(accountsRepository);
        server.start();

        ObjectMapper mapper = new ObjectMapper();
        RestAssured.port = 8011;

        // when
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(mapper.writeValueAsBytes(new AccountDto(null, new BigDecimal(10))))
                .post("/api/v1/accounts");

        // then
        response
                .then()
                .statusCode(201)
                .body("balance", is(10));


        List<Account> accounts = new ArrayList(accountsRepository.getAll());
        assertThat(accounts.size(), is(1));
        assertThat(accounts.get(0).getBalance(), is(Money.of(10)));
    }

    @Test
    public void whenGetAccountIsCalledForExistingAccount_then200WithAccountInformationIsReturned() throws IOException {
        // given
        var accountsRepository = new InMemoryAccountsRepository();
        server = startServer(accountsRepository);
        server.start();

        Account account = accountsService.addNewAccount(Money.of(11));

        // when
        Response response = given()
                .accept(ContentType.JSON)
                .get("/api/v1/accounts/" + account.getAccountId().getId().toString());

        // then
        response
                .then()
                .statusCode(200)
                .body("id", is(account.getAccountId().getId().toString()))
                .body("balance", is(11));

    }

    @Test
    public void whenGetAccountIsCalledForNotExistingAccount_then404NotFoundIsReturned() throws IOException {
        // given
        var accountsRepository = new InMemoryAccountsRepository();
        server = startServer(accountsRepository);
        server.start();

        UUID accountId = UUID.randomUUID();

        // when
        Response response = given()
                .accept(ContentType.JSON)
                .get("/api/v1/accounts/" + accountId);

        // then
        String message = String.format("Account with id=%s does not exist.", accountId);
        response
                .then()
                .statusCode(404)
                .body("message", is(message));
    }

    @Test
    public void whenGetAccountsIsCalled_then200WithExistingAccountsAreReturned() throws IOException {
        // given
        var accountsRepository = mock(AccountsRepository.class);
        server = startServer(accountsRepository);
        server.start();

        var account1 = new Account(AccountId.createRandom(), Money.of(11));
        var account2 = new Account(AccountId.createRandom(), Money.of(12));

        when(accountsRepository.getAll()).thenReturn(Arrays.asList(account1, account2));

        // when
        Response response = given()
                .accept(ContentType.JSON)
                .get("/api/v1/accounts");

        // then
        response
                .then()
                .statusCode(200)
                .body("id", containsInAnyOrder(
                        account1.getAccountId().getId().toString(),
                        account2.getAccountId().getId().toString()
                ))
                .body("balance", containsInAnyOrder(11, 12));
    }

    private HttpServerExtended startServer(AccountsRepository accountsRepository) throws IOException {
        accountsService = new AccountsServiceImpl(accountsRepository);
        AccountsResourceRequestHandlers accountsHandlers = new AccountsResourceRequestHandlers(accountsService);

//        TransfersServiceImpl transfersService = new TransfersServiceImpl(accountsService);
//        TransfersResourceRequestHandler transfersHandlers = new TransfersResourceRequestHandler(transfersService);

        return
                new HttpServerBuilder()
                        .withMapping("/api/v1/accounts", "/.*", HttpVerb.GET, accountsHandlers::handleGetAccount)
                        .withMapping("/api/v1/accounts", HttpVerb.GET, accountsHandlers::handleGetAccounts)
                        .withMapping("/api/v1/accounts", HttpVerb.POST, accountsHandlers::handlePostAccount)
                        //.withMapping("/api/v1/transfers", HttpVerb.POST, transfersHandlers::handlePostTransfer)
                        .build(8011);
    }
}
