package org.ack.http;

import org.ack.http.serialization.HttpSerializer;
import org.ack.http.serialization.JsonHttpSerializer;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class HttpServerBuilder {

    private static Logger log = LogManager.getLogger(HttpServerBuilder.class);

    static class Mapping {
        public String path;
        public RequestMatcher matcher;
        public RequestHandler requestHandler;

        public Mapping(String path, RequestMatcher matcher, RequestHandler requestHandler) {
            this.path = path;
            this.matcher = matcher;
            this.requestHandler = requestHandler;
        }

        public String getPath() {
            return path;
        }

        public RequestMatcher getMatcher() {
            return matcher;
        }

        public RequestHandler getRequestHandler() {
            return requestHandler;
        }
    }

    private List<Mapping> mappings = new ArrayList<>();
    private ResponseFailureHandler errorHandler;
    private ResponseSuccessHandler successHandler;
    private RequestReader requestReader;
    private ResponseWriter responseWriter;
    private HttpSerializer httpSerializer;

    public HttpServerBuilder withMapping(String path, HttpVerb verb, RequestHandler requestHandler) {
        Validate.notNull(verb);
        Validate.notNull(requestHandler);

        RequestMatcher requestMatcher = new RequestMatcher(path, verb);

        mappings.add(new Mapping(path, requestMatcher, requestHandler));

        return this;
    }

    public HttpServerBuilder withMapping(String path, String argsPattern, HttpVerb verb, RequestHandler requestHandler) {
        Validate.notNull(verb);
        Validate.notEmpty(argsPattern);
        Validate.notNull(requestHandler);

        RequestMatcher requestMatcher = new RequestMatcher(path + argsPattern, verb);

        mappings.add(new Mapping(path, requestMatcher, requestHandler));

        return this;
    }

    public HttpServerBuilder withErrorHandler(ResponseFailureHandler errorHandler) {
        Validate.notNull(errorHandler);

        this.errorHandler = errorHandler;

        return this;
    }


    public HttpServerExtended build(int port) throws IOException {
        if (httpSerializer == null) {
            httpSerializer = new JsonHttpSerializer();
        }
        if (requestReader == null) {
            requestReader = new RequestReader(httpSerializer);
        }
        if (responseWriter == null) {
            responseWriter = new ResponseWriter(httpSerializer);
        }
        if (successHandler == null) {
            successHandler = new DefaultResponseSuccessHandler(responseWriter);
        }
        if (errorHandler == null)
            errorHandler = new DefaultResponseFailureHandler(responseWriter);

        log.debug("Configuring org.ack.http server with settings:");
        log.debug("HttpSerializer   : {}", httpSerializer.getClass().getName());
        log.debug("RequestReader    : {}", requestReader.getClass().getName());
        log.debug("ResponseWriter   : {}", responseWriter.getClass().getName());
        log.debug("ResponseSuccessHandler   : {}", successHandler.getClass().getName());
        log.debug("ResponseFailureHandler   : {}", errorHandler.getClass().getName());

        Collection<RoutingHttpHandler> routingHttpHandlers = registerRequestHandlers();

        routingHttpHandlers.stream().forEach(routingHandler -> {
            routingHandler.getHandlers().forEach(requestHandler -> log.debug(
                    "Registering handler  : uri expression -> {}, verb -> {}, class => {}",
                    requestHandler.getRequestMatcher().getUriExpression(),
                    requestHandler.getRequestMatcher().getVerb(),
                    requestHandler.getRequestHandler().getClass().getName()
            ));
        });

        return new HttpServerExtended(port, routingHttpHandlers);
    }

    private Collection<RoutingHttpHandler> registerRequestHandlers() {
        Map<String, List<Mapping>> mappingsPerPath = mappings.stream().collect(groupingBy(Mapping::getPath));
        return
                mappingsPerPath
                        .entrySet()
                        .stream()
                        .map(entry ->
                                new RoutingHttpHandler(
                                        entry.getKey(),
                                        entry.getValue().stream().map(this::createCoreHandler).collect(toList()),
                                        errorHandler
                                )
                        )
                        .collect(toList());
    }

    private CoreHttpHandler createCoreHandler(Mapping mapping) {
        return new CoreHttpHandler(mapping.matcher, mapping.requestHandler, successHandler, errorHandler, requestReader, responseWriter);
    }
}
