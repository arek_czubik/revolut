package org.ack.http;

import com.sun.net.httpserver.HttpExchange;

/**
 * Handles reaction to success while handling HTTP requests.
 */
public interface ResponseSuccessHandler {

    void onSuccess(HttpExchange exchange);
    void onSuccess(HttpExchange exchange, int statusCode, Object object);

}
