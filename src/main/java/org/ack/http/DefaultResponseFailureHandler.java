package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import org.ack.http.exception.BadRequestException;
import org.ack.http.exception.NotAcceptableResponseTypeException;
import org.ack.http.exception.UnsupportedMediaTypeException;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Separates other components from decisions about shape of response and response status code.
 * <p>
 * On the other hand it is separated from raw response manipulation by {@link ResponseWriter}
 */
public class DefaultResponseFailureHandler implements ResponseFailureHandler {

    private static Logger log = LogManager.getLogger(DefaultResponseFailureHandler.class);
    private final ResponseWriter responseWriter;

    public DefaultResponseFailureHandler(ResponseWriter responseWriter) {
        this.responseWriter = responseWriter;
    }

    @Override
    public void onMethodNotSupported(HttpExchange exchange) {
        responseWriter.write(exchange, HttpStatus.SC_NOT_FOUND, null);
    }

    @Override
    public void onBadRequest(HttpExchange exchange, BadRequestException exception) {
        String message = String.format("Bad request. Cause: %s", exception.getMessage());
        HttpFailureResponse httpFailureResponse = new HttpFailureResponse(HttpStatus.SC_BAD_REQUEST, message, System.currentTimeMillis());
        responseWriter.write(exchange, httpFailureResponse.getStatusCode(), httpFailureResponse);
    }

    @Override
    public void onUnsupportedMediaType(HttpExchange exchange, UnsupportedMediaTypeException exception) {
        String message = String.format("Unsupported media type. Cause: %s", exception.getMessage());
        HttpFailureResponse httpFailureResponse = new HttpFailureResponse(HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE, message, System.currentTimeMillis());
        responseWriter.write(exchange, httpFailureResponse.getStatusCode(), httpFailureResponse);
    }

    @Override
    public void onNotAcceptableResponseType(HttpExchange exchange, NotAcceptableResponseTypeException exception) {
        String message = String.format("Not acceptable response type. Cause: %s", exception.getMessage());
        HttpFailureResponse httpFailureResponse = new HttpFailureResponse(HttpStatus.SC_NOT_ACCEPTABLE, message, System.currentTimeMillis());
        responseWriter.write(exchange, httpFailureResponse.getStatusCode(), httpFailureResponse);
    }

    @Override
    public void onException(HttpExchange exchange, Exception exception) {
        if (exception instanceof BadRequestException) {
            log.error(String.format("Bad request : path=%s, verb=%s", exchange.getHttpContext().getPath(), exchange.getRequestMethod()));
            onBadRequest(exchange, BadRequestException.class.cast(exception));
        } else if (exception instanceof NotAcceptableResponseTypeException) {
            log.error(String.format("Not acceptable response type : path=%s, verb=%s", exchange.getHttpContext().getPath(), exchange.getRequestMethod()));
            onNotAcceptableResponseType(exchange, NotAcceptableResponseTypeException.class.cast(exception));
        } else if (exception instanceof UnsupportedMediaTypeException) {
            log.error(String.format("Unsupported media type : path=%s, verb=%s", exchange.getHttpContext().getPath(), exchange.getRequestMethod()));
            onUnsupportedMediaType(exchange, UnsupportedMediaTypeException.class.cast(exception));
        } else {
            log.error(String.format("Request failure : path=%s, verb=%s", exchange.getHttpContext().getPath(), exchange.getRequestMethod()));
            onFailure(exchange, exception);
        }
    }

    @Override
    public void onFailure(HttpExchange exchange, Exception exception) {
        HttpFailureResponse httpFailureResponse =
                new HttpFailureResponse(
                        HttpStatus.SC_INTERNAL_SERVER_ERROR,
                        exception.getMessage(),
                        System.currentTimeMillis()
                );
        responseWriter.write(exchange, httpFailureResponse.getStatusCode(), httpFailureResponse);
    }

    @Override
    public void onFailure(HttpExchange exchange, HttpFailureResponse response) {
        responseWriter.write(exchange, response.getStatusCode(), response);
    }
}
