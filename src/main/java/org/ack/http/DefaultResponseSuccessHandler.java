package org.ack.http;

import com.sun.net.httpserver.HttpExchange;

public class DefaultResponseSuccessHandler implements ResponseSuccessHandler {

    private final ResponseWriter responseWriter;

    public DefaultResponseSuccessHandler(ResponseWriter responseWriter) {
        this.responseWriter = responseWriter;
    }

    @Override
    public void onSuccess(HttpExchange exchange) {
        try {

            responseWriter.write(exchange, 200, null);

        } catch (Exception exception) {

        }

    }

    @Override
    public void onSuccess(HttpExchange exchange, int statusCode, Object object) {
        responseWriter.write(exchange, statusCode, object);
    }

}
