package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.ack.http.exception.NotAcceptableResponseTypeException;
import org.ack.http.exception.UnsupportedMediaTypeException;
import org.apache.commons.lang3.Validate;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Responsible for:
 * - handling failure and success result from {@link RequestHandler} call
 * - verifying Content-Type and Accept headers with available serializers
 */
public class CoreHttpHandler implements HttpHandler {

    private static Logger log = LogManager.getLogger(CoreHttpHandler.class);

    private final RequestHandler requestHandler;
    private final ResponseSuccessHandler successHandler;
    private final ResponseFailureHandler failureHandler;
    private final RequestReader requestReader;
    private final ResponseWriter responseWriter;
    private final RequestMatcher requestMatcher;

    public CoreHttpHandler(
            RequestMatcher requestMatcher,
            RequestHandler requestHandler,
            ResponseSuccessHandler successHandler,
            ResponseFailureHandler failureHandler,
            RequestReader requestReader,
            ResponseWriter responseWriter) {

        Validate.notNull(requestMatcher);
        Validate.notNull(requestHandler);
        Validate.notNull(successHandler);
        Validate.notNull(failureHandler);
        Validate.notNull(requestReader);
        Validate.notNull(responseWriter);

        this.requestMatcher = requestMatcher;
        this.requestHandler = requestHandler;
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
        this.requestReader = requestReader;
        this.responseWriter = responseWriter;
    }

    public RequestMatcher getRequestMatcher() {
        return requestMatcher;
    }

    public RequestHandler getRequestHandler() {
        return requestHandler;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        try {
            log.debug(String.format("Request start : path=%s, verb=%s", exchange.getRequestURI().getPath(), exchange.getRequestMethod()));

            verifyContentType(exchange);
            verifyAccept(exchange);

            Object response = requestHandler.handle(exchange, requestReader);

            if (response == null) {
                successHandler.onSuccess(exchange);
            } else if (response instanceof HttpSuccessResponse) {
                var casted = HttpSuccessResponse.class.cast(response);
                successHandler.onSuccess(exchange, casted.getHttpStatus(), casted.getData());
            } else if (response instanceof HttpFailureResponse) {
                failureHandler.onFailure(exchange, HttpFailureResponse.class.cast(response));
            } else {
                successHandler.onSuccess(exchange, HttpStatus.SC_OK, response);
            }

            log.debug(String.format("Request completed : path=%s, verb=%s", exchange.getRequestURI().getPath(), exchange.getRequestMethod()));
        } catch (Exception exception) {
            failureHandler.onException(exchange, exception);
        }

        exchange.close();
    }

    private void verifyAccept(HttpExchange exchange) {
        List<String> acceptTypes = exchange.getRequestHeaders().get("Accept");
        if (acceptTypes != null
                && acceptTypes.size() >= 0
                && !acceptTypes.get(0).equals("*/*")
                && !requestReader.isContentTypeSupported(acceptTypes.get(0))) {
            throw new NotAcceptableResponseTypeException(acceptTypes.get(0));
        }
    }

    private void verifyContentType(HttpExchange exchange) {
        List<String> contentTypes = exchange.getRequestHeaders().get("Content-Type");
        if (contentTypes != null && !requestReader.isContentTypeSupported(contentTypes.get(0))) {
            throw new UnsupportedMediaTypeException(contentTypes.get(0));
        }
    }
}
