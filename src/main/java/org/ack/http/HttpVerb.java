package org.ack.http;

public enum HttpVerb {
    GET,
    POST,
    PUT,
    DELETE
}
