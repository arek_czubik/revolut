package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import org.ack.http.exception.BadRequestException;
import org.ack.http.serialization.HttpSerializer;
import org.apache.commons.lang3.Validate;

public class RequestReader {

    private HttpSerializer httpSerializer;

    public RequestReader(HttpSerializer httpSerializer) {
        Validate.notNull(httpSerializer);

        this.httpSerializer = httpSerializer;
    }

    public <T> T readBodyAs(HttpExchange exchange, Class<T> type) {
        try {
            byte[] bytes = exchange.getRequestBody().readAllBytes();
            return httpSerializer.deserialize(bytes, type);
        } catch (Exception exception) {
            throw new BadRequestException(String.format("Failed to read body. Cause: %s", exception.getMessage()), exception);
        }

    }

    public boolean isContentTypeSupported(String contentType) {
        return httpSerializer.accepts(contentType);
    }
}
