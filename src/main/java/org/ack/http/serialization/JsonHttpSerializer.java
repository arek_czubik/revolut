package org.ack.http.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ack.http.serialization.HttpSerializer;
import org.ack.http.serialization.SerializationException;

import java.io.IOException;
import java.io.InputStream;

public class JsonHttpSerializer implements HttpSerializer {
    @Override
    public boolean accepts(String contentType) {
        return contentType.toLowerCase().startsWith("application/json");
    }

    @Override
    public byte[] serialize(Object data) {
        try {
            return new ObjectMapper().writeValueAsBytes(data);
        } catch (IOException e) {
            throw new SerializationException(String.format("Failed to serialize value"), e);
        }
    }

    @Override
    public <T> T deserialize(InputStream data, Class<T> type) {
        try {
            return new ObjectMapper().readValue(data, type);
        } catch (IOException e) {
            throw new SerializationException(String.format("Failed to deserialize type %s", type.getName()), e);
        }
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> type) {
        try {
            return new ObjectMapper().readValue(data, type);
        } catch (IOException e) {
            throw new SerializationException(String.format("Failed to deserialize type %s", type.getName()), e);
        }
    }

    @Override
    public String getContentType() {
        return "application/json";
    }
}
