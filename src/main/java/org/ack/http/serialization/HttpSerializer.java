package org.ack.http.serialization;

import java.io.InputStream;

/**
 * Responsible for serialization and deserialization of data.
 */
public interface HttpSerializer {

    /**
     * Returns true if supports serialization of provided MIME type
     */
    boolean accepts(String contentType);

    /**
     * Returns MIME type used to set Content-Type header in response.
     */
    String getContentType();

    /**
     * Serializes provided data.
     */
    byte[] serialize(Object data);

    /**
     * Deserializes from provided stream instance of provided type
     */
    <T> T deserialize(InputStream data, Class<T> type);
    <T> T deserialize(byte[] data, Class<T> type);

}
