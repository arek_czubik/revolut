package org.ack.http;

import org.apache.http.HttpStatus;

public class HttpSuccessResponse<T> {
    private final int httpStatus;
    private final long timestamp;
    private final T data;

    public HttpSuccessResponse(int httpStatus, long timestamp, T data) {
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
        this.data = data;
    }

    public static <T> HttpSuccessResponse<T> ok(T data) {
        return new HttpSuccessResponse<>(HttpStatus.SC_OK, System.currentTimeMillis(), data);
    }

    public static <T> HttpSuccessResponse<T> created(T data) {
        return new HttpSuccessResponse<>(HttpStatus.SC_CREATED, System.currentTimeMillis(), data);
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public T getData() {
        return data;
    }
}
