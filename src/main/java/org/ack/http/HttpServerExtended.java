package org.ack.http;

import com.sun.net.httpserver.HttpServer;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;

public class HttpServerExtended  {
    private static Logger log = LogManager.getLogger(HttpServerExtended.class);

    private final List<RoutingHttpHandler> methodRouters;
    private final HttpServer httpServer;
    private final int port;

    public HttpServerExtended(int port, Collection<RoutingHttpHandler> methodRouters) throws IOException {
        Validate.notNull(methodRouters);

        this.port = port;
        this.methodRouters = new ArrayList<>(methodRouters);
        this.httpServer = HttpServer.create(new InetSocketAddress(port), 0);
        this.httpServer.setExecutor(Executors.newFixedThreadPool(4));

        methodRouters.stream().forEach( verbRouter -> {
            log.debug(String.format("Registering routing handler: path -> %s", verbRouter.getPath()));
            httpServer.createContext(verbRouter.getPath(), verbRouter);
        });
    }

    public void start() {
        log.info("Starting server at port = {}", port);
        try {
            httpServer.start();
        } catch (Exception exception) {
            log.error("Failed to start server at port = {}", port);
            throw exception;
        }

    }

    public void stop(int delay) {
        httpServer.stop(delay);
    }

    public List<RoutingHttpHandler> getMethodRouters() {
        return methodRouters;
    }

}
