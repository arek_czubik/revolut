package org.ack.http;

import org.apache.http.HttpStatus;

public class HttpFailureResponse {
    private final int statusCode;
    private final String message;
    private final long timestamp;

    public HttpFailureResponse(int statusCode, String message, long timestamp) {
        this.statusCode = statusCode;
        this.message = message;
        this.timestamp = timestamp;
    }

    public static HttpFailureResponse notFound(String message) {
        return new HttpFailureResponse(HttpStatus.SC_NOT_FOUND, message, System.currentTimeMillis());
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public long getTimestamp() {
        return timestamp;
    }
}

