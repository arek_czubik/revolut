package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import org.ack.http.serialization.HttpSerializer;
import org.apache.commons.lang3.Validate;

public class ResponseWriter {

    private final HttpSerializer serializer;

    public ResponseWriter(HttpSerializer serializer) {
        Validate.notNull(serializer);
        this.serializer = serializer;
    }

    public void write(HttpExchange exchange, int httpStatus, Object responseData) {
        try {
            byte[] responseBytes = serializer.serialize(responseData);
            exchange.getResponseHeaders().add("Content-Type", serializer.getContentType());
            exchange.sendResponseHeaders(httpStatus, responseBytes.length);
            exchange.getResponseBody().write(responseBytes);
        } catch (Exception exception) {
            throw new RuntimeException("don't know yet what to do here");
        }
    }
}
