package org.ack.http;

import com.sun.net.httpserver.HttpExchange;

/**
 * Handles HTTP request expressed as HttpExchange.
 */
@FunctionalInterface
public interface RequestHandler {

    /**
     * Handles HTTP request.
     *
     * Supported responses/exceptions with default actions taken:
     *
     * when returned
     * - null                   -> successHandler.onSuccess(HttpExchange )
     * - HttpSuccessResponse    -> successHandler.onSuccess(HttpExchange , HttpSuccessResponse )
     * - HttpFailureResponse    -> failureHandler.onFailure(HttpExchange , HttpFailureResponse )
     * - Object                 -> successHandler.onSuccess(HttpExchange , Object )
     *
     * when thrown
     * - Exception              -> failureHandler.onFailure(HttpExchange , Exception )
     * - BadRequestException    -> failureHandler.onBadRequest(HttpExchange , BadRequestException )
     */
    Object handle(HttpExchange exchange, RequestReader requestReader);
}
