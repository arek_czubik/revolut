package org.ack.http;

import org.apache.commons.lang3.Validate;

import java.util.regex.Pattern;

public class RequestMatcher {
    private final String uriExpression;
    private final Pattern uriPattern;
    private final HttpVerb verb;

    public RequestMatcher(String uriExpression, HttpVerb verb) {
        Validate.notNull(uriExpression);
        Validate.notNull(verb);

        this.uriExpression = uriExpression;
        this.uriPattern = Pattern.compile(uriExpression);
        this.verb = verb;
    }

    public boolean matches(String requestPath, HttpVerb verb) {
        Validate.notNull(requestPath);
        Validate.notNull(verb);

        return verb.equals(this.verb) && uriPattern.matcher(requestPath).matches();
    }

    public String getUriExpression() {
        return uriExpression;
    }

    public Pattern getUriPattern() {
        return uriPattern;
    }

    public HttpVerb getVerb() {
        return verb;
    }
}
