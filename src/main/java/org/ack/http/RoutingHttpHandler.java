package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

/**
 * Responsibility:
 * 1. Routes a request to {@link RequestHandler} registered for this path for this verb.
 * 2. If no handle is registered it is delegated to {@link ResponseFailureHandler} as methodNotSupported call.
 *
 */
public class RoutingHttpHandler implements HttpHandler {

    private final Collection<CoreHttpHandler> handlers;
    private final ResponseFailureHandler errorHandler;
    private final String path;

    public RoutingHttpHandler(
            String path,
            Collection<CoreHttpHandler> handlers,
            ResponseFailureHandler errorHandler) {

        Validate.notEmpty(path);
        Validate.notNull(handlers);
        Validate.notNull(errorHandler);

        this.path = path;
        this.errorHandler = errorHandler;
        this.handlers = handlers;
    }

    public String getPath() {
        return path;
    }

    public Collection<CoreHttpHandler> getHandlers() {
        return handlers;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestUriPath = exchange.getRequestURI().getPath();
        HttpVerb verb = HttpVerb.valueOf(exchange.getRequestMethod());
        Optional<CoreHttpHandler> matchingHandler = handlers
                .stream()
                .filter(matcher -> matcher.getRequestMatcher().matches(requestUriPath, verb))
                .findFirst();

        if (matchingHandler.isPresent()) {
            matchingHandler.get().handle(exchange);
        } else {
            errorHandler.onMethodNotSupported(exchange);
        }
    }
}
