package org.ack.http.exception;

public class NotAcceptableResponseTypeException extends RuntimeException {

    public NotAcceptableResponseTypeException(String message) {
        super(message);
    }

    public NotAcceptableResponseTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
