package org.ack.http;

import com.sun.net.httpserver.HttpExchange;
import org.ack.http.exception.BadRequestException;
import org.ack.http.exception.NotAcceptableResponseTypeException;
import org.ack.http.exception.UnsupportedMediaTypeException;

/**
 * Handles reaction to failures while handling HTTP requests.
 */
public interface ResponseFailureHandler {

    void onFailure(HttpExchange exchange, HttpFailureResponse response);

    void onFailure(HttpExchange exchange, Exception exception);
    void onMethodNotSupported(HttpExchange exchange);
    void onBadRequest(HttpExchange exchange, BadRequestException exception);
    void onUnsupportedMediaType(HttpExchange exchange, UnsupportedMediaTypeException exception);
    void onNotAcceptableResponseType(HttpExchange exchange, NotAcceptableResponseTypeException exception);

    void onException(HttpExchange exchange, Exception exception);
}
