package org.ack;

import org.ack.domain.accounts.business.impl.AccountsServiceImpl;
import org.ack.domain.accounts.business.impl.InMemoryAccountsRepository;
import org.ack.domain.accounts.business.impl.TransfersServiceImpl;
import org.ack.domain.accounts.interfaces.rest.AccountsResourceRequestHandlers;
import org.ack.domain.accounts.interfaces.rest.TransfersResourceRequestHandler;
import org.ack.http.HttpServerBuilder;
import org.ack.http.HttpServerExtended;
import org.ack.http.HttpVerb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class TransfersRestServiceApplication {

    private static Logger log = LogManager.getLogger(TransfersRestServiceApplication.class);
    private static final int DEFAULT_PORT = 8011;

    public static void main(String[] args) throws IOException {

        int port = DEFAULT_PORT;
        if (args.length == 1) {
            port = Integer.valueOf(args[0]);
        }

        InMemoryAccountsRepository inMemoryAccountsRepository = new InMemoryAccountsRepository();
        AccountsServiceImpl accountsService = new AccountsServiceImpl(inMemoryAccountsRepository);
        AccountsResourceRequestHandlers accountsResourceRequestHandlers = new AccountsResourceRequestHandlers(accountsService);

        TransfersServiceImpl transfersService = new TransfersServiceImpl(accountsService);
        TransfersResourceRequestHandler transfersResourceRequestHandler = new TransfersResourceRequestHandler(transfersService);

        HttpServerExtended server = new HttpServerBuilder()
                .withMapping("/api/v1/accounts", "/.+", HttpVerb.GET, accountsResourceRequestHandlers::handleGetAccount)
                .withMapping("/api/v1/accounts", HttpVerb.GET, accountsResourceRequestHandlers::handleGetAccounts)
                .withMapping("/api/v1/accounts", HttpVerb.POST, accountsResourceRequestHandlers::handlePostAccount)
                .withMapping("/api/v1/transfers", HttpVerb.POST, transfersResourceRequestHandler::handlePostTransfer)
                .build(port);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Stopping server...");
            server.stop(1);
        }));

        server.start();

    }

}
