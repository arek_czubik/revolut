package org.ack.domain.accounts.interfaces.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.ack.domain.accounts.business.api.Account;
import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;

class AccountDto {
    private final String id;
    private final BigDecimal balance;

    public AccountDto(
            @JsonProperty("id") String id,
            @JsonProperty("balance") BigDecimal balance) {
        Validate.notNull(balance);

        this.id = id;
        this.balance = balance;
    }

    public static AccountDto of(Account account) {
        return new AccountDto(account.getAccountId().getId().toString(), account.getBalance().getAmount());
    }

    public String getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
