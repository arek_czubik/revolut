package org.ack.domain.accounts.interfaces.rest;

import com.sun.net.httpserver.HttpExchange;
import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.ack.http.HttpFailureResponse;
import org.ack.http.HttpSuccessResponse;
import org.ack.http.RequestReader;
import org.ack.http.ResponseFailureHandler;

import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

/**
 * Handler for Accounts resource calls.
 */
public class AccountsResourceRequestHandlers {
    private final AccountsService accountsService;

    public AccountsResourceRequestHandlers(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    /**
     * Handler of /api/v1/accounts/{accountId} GET call.
     */
    public Object handleGetAccount(HttpExchange exchange, RequestReader requestReader) {
        String accountIdString = getAccountIdFromUri(exchange.getRequestURI());
        AccountId accountId = AccountId.of(accountIdString);
        Optional<Account> account = accountsService.findById(accountId);

        if (!account.isPresent()) {
            return HttpFailureResponse.notFound(
                    String.format("Account with id=%s does not exist.",
                            accountId.getId().toString())
            );
        }

        AccountDto result =
                new AccountDto(account.get().getAccountId().getId().toString(), account.get().getBalance().getAmount());
        return HttpSuccessResponse.ok(result);
    }

    public Object handleGetAccounts(HttpExchange exchange, RequestReader requestReader) {
        return accountsService.getAll()
                .stream()
                .map(account -> AccountDto.of(account))
                .collect(toList());
    }

    public Object handlePostAccount(HttpExchange exchange, RequestReader requestReader) {
        AccountDto accountDto = requestReader.readBodyAs(exchange, AccountDto.class);
        Account newAccount = accountsService.addNewAccount(new Money(accountDto.getBalance()));
        return HttpSuccessResponse.created(AccountDto.of(newAccount));
    }

    private String getAccountIdFromUri(URI requestUri) {
        Matcher matcher = Pattern.compile("/api/v1/accounts/(?<accountId>.*)").matcher(requestUri.toString());
        matcher.find();
        return matcher.group("accountId");
    }
}
