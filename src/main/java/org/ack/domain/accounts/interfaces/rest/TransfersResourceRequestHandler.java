package org.ack.domain.accounts.interfaces.rest;

import com.sun.net.httpserver.HttpExchange;
import org.ack.domain.accounts.business.api.Transfer;
import org.ack.domain.accounts.business.api.TransfersService;
import org.ack.domain.accounts.business.impl.AccountNotFoundException;
import org.ack.http.HttpFailureResponse;
import org.ack.http.HttpSuccessResponse;
import org.ack.http.RequestReader;

/**
 * Handler for Transfers resource calls.
 */
public class TransfersResourceRequestHandler {
    private final TransfersService transfersService;

    public TransfersResourceRequestHandler(TransfersService transfersService) {
        this.transfersService = transfersService;
    }

    /**
     * Handler of /api/v1/transfers POST call.
     */
    public Object handlePostTransfer(HttpExchange exchange, RequestReader requestReader) {

        Transfer transfer = null;
        transfer = requestReader.readBodyAs(exchange, TransferDto.class).asTransfer();

        try {
            transfersService.transfer(
                    transfer.getSourceAccountId(),
                    transfer.getDestinationAccountId(),
                    transfer.getAmountToBeTransferred()
            );
        } catch (AccountNotFoundException exception) {
            return HttpFailureResponse.notFound(exception.getMessage());
        }

        return HttpSuccessResponse.ok(null);
    }
}
