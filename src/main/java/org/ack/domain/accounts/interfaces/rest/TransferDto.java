package org.ack.domain.accounts.interfaces.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.Money;
import org.ack.domain.accounts.business.api.Transfer;
import org.apache.commons.lang3.Validate;

public class TransferDto {
    private final String sourceAccountId;
    private final String destinationAccountId;
    private final String amountToBeTransferred;

    public TransferDto(
            @JsonProperty("sourceAccountId") String from,
            @JsonProperty("destinationAccountId") String to,
            @JsonProperty("amountToBeTransferred") String amount) {

        Validate.notNull(from);
        Validate.notNull(to);
        Validate.notNull(amount);

        sourceAccountId = from;
        destinationAccountId = to;
        amountToBeTransferred = amount;
    }

    public Transfer asTransfer() {
        return
                new Transfer(
                        AccountId.of(this.sourceAccountId),
                        AccountId.of(this.destinationAccountId),
                        Money.of(this.amountToBeTransferred));
    }

    public String getSourceAccountId() {
        return sourceAccountId;
    }

    public String getDestinationAccountId() {
        return destinationAccountId;
    }

    public String getAmountToBeTransferred() {
        return amountToBeTransferred;
    }
}
