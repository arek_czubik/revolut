package org.ack.domain.accounts.business.impl;

import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.ack.domain.accounts.business.api.TransfersService;
import org.apache.commons.lang3.Validate;

import java.util.Optional;


public class TransfersServiceImpl implements TransfersService {
    private final AccountsService accountsService;

    public TransfersServiceImpl(AccountsService accountsService) {
        Validate.notNull(accountsService);

        this.accountsService = accountsService;
    }

    public void transfer(AccountId fromAccountId, AccountId toAccountId, Money amount) {
        Validate.notNull(fromAccountId);
        Validate.notNull(toAccountId);
        Validate.notNull(amount);

        Optional<Account> from = accountsService.findById(fromAccountId);
        Optional<Account> to = accountsService.findById(toAccountId);

        verifyAccount(from, fromAccountId);
        verifyAccount(to, toAccountId);
        verifySourceAccountHasEnoughMoneyToMakeATransfer(from.get(), amount);

        from.get().transferMoneyTo(to.get(), amount);
    }

    private void verifySourceAccountHasEnoughMoneyToMakeATransfer(Account account, Money requiredBalance) {
        if (account.getBalance().getAmount().compareTo(requiredBalance.getAmount()) < 0) {
            throw new RuntimeException(String.format("Account with id=%s has not enough money to transfer %s", account.getAccountId(), requiredBalance.getAmount()));
        }
    }

    private void verifyAccount(Optional<Account> account, AccountId requestedId) {
        if (!account.isPresent()) {
            throw new AccountNotFoundException(String.format("Account with id %s does not exist", requestedId));
        }
    }
}
