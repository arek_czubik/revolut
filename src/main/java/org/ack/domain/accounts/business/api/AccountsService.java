package org.ack.domain.accounts.business.api;

import java.util.Collection;
import java.util.Optional;

public interface AccountsService {
    Optional<Account> findById(AccountId accountId);

    Account addNewAccount(Money balance);

    Collection<Account> getAll();
}

