package org.ack.domain.accounts.business.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    private final AccountId accountId;
    private Money balance;
    private final ReentrantLock lock = new ReentrantLock();

    public Account(AccountId accountId, Money balance) {
        Validate.notNull(accountId);
        Validate.notNull(balance);

        this.accountId = accountId;
        this.balance = balance;
    }

    public Account(AccountId accountId) {
        this(accountId, new Money(BigDecimal.valueOf(0)));
    }

    public AccountId getAccountId() {
        return accountId;
    }

    public Money getBalance() {
        return balance;
    }

    public void transferMoneyTo(Account destinationAccount, Money amount) {
        Validate.notNull(destinationAccount);
        Validate.notNull(amount);
        Validate.validState(!destinationAccount.equals(this), "Cannot transfer money within one account.");

        boolean thisLock = false;
        boolean otherLock = false;
        try {
            thisLock = lock.tryLock(100, TimeUnit.MILLISECONDS);
            otherLock = destinationAccount.lock.tryLock(100, TimeUnit.MILLISECONDS);

            this.setBalance(this.getBalance().getAmount().subtract(amount.getAmount()));
            destinationAccount.setBalance(destinationAccount.getBalance().getAmount().add(amount.getAmount()));

        } catch (InterruptedException e) {

        } finally {
            if (otherLock) {
                destinationAccount.lock.unlock();
            }
            if (thisLock) {
                lock.unlock();
            }
        }
    }

    private void setBalance(BigDecimal newBalance) {
        Validate.validState(newBalance.compareTo(BigDecimal.ZERO) >= 0, "Account balance cannot be smaller than 0");

        balance = new Money(newBalance);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", balance=" + balance +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof  Account)) return false;

        Account other = (Account )obj;
        return Objects.equals(this.accountId, other.accountId);
    }
}
