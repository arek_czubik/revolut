package org.ack.domain.accounts.business.impl;

import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsRepository;
import org.apache.commons.lang3.Validate;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class InMemoryAccountsRepository implements AccountsRepository {
    private final ConcurrentHashMap<AccountId, Account> accounts = new ConcurrentHashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public Optional<Account> findById(AccountId accountId) {
        Validate.notNull(accountId);

        Lock lock = null;
        try {
            if (accounts.containsKey(accountId)) {
                return Optional.of(accounts.get(accountId));
            } else {
                return Optional.empty();
            }
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    public Account add(Account account) {
        Validate.notNull(account);

        Lock lock = null;
        try {
            this.lock.writeLock();
            accounts.put(account.getAccountId(), account);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }

        return account;
    }

    @Override
    public  Collection<Account> getAll() {
        Lock lock = null;
        try {
            this.lock.readLock();
            return new ArrayList(accounts.values());
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }
}
