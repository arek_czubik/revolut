package org.ack.domain.accounts.business.api;

import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;
import java.util.Objects;

public class Money {
    private final BigDecimal amount;

    public Money(BigDecimal amount) {
        Validate.notNull(amount);

        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public static Money of(int balance) {
        return new Money(new BigDecimal(balance));
    }

    public static Money of(String moneyString) {
        Validate.notEmpty(moneyString);

        return new Money(new BigDecimal(moneyString));
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(amount);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Money)) return false;

        Money other = (Money )obj;
        return Objects.equals(this.amount, other.amount);
    }
}
