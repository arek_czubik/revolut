package org.ack.domain.accounts.business.api;

public interface TransfersService {
    void transfer(AccountId fromAccountId, AccountId toAccountId, Money amount);
}


