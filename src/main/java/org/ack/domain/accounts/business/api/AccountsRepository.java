package org.ack.domain.accounts.business.api;

import java.util.Collection;
import java.util.Optional;

public interface AccountsRepository {
    Optional<Account> findById(AccountId accountId);
    Account add(Account account);

    Collection<Account> getAll();
}

