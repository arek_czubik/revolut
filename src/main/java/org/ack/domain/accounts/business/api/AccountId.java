package org.ack.domain.accounts.business.api;

import org.apache.commons.lang3.Validate;

import java.util.Objects;
import java.util.UUID;

public class AccountId {
    private final UUID accountId;

    public AccountId(UUID accountId) {
        Validate.notNull(accountId);

        this.accountId = accountId;
    }

    public static AccountId of(String accountIdString) {
        return new AccountId(UUID.fromString(accountIdString));
    }
    public static AccountId createRandom() {
        return new AccountId(UUID.randomUUID());
    }

    public UUID getId() {
        return accountId;
    }

    @Override
    public String toString() {
        return accountId.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(accountId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof AccountId))
            return false;
        AccountId other = (AccountId)obj;
        return this.accountId.equals(other.accountId);
    }
}
