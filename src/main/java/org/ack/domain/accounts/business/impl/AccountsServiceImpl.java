package org.ack.domain.accounts.business.impl;

import org.ack.domain.accounts.business.api.Account;
import org.ack.domain.accounts.business.api.AccountId;
import org.ack.domain.accounts.business.api.AccountsRepository;
import org.ack.domain.accounts.business.api.AccountsService;
import org.ack.domain.accounts.business.api.Money;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        Validate.notNull(accountsRepository);

        this.accountsRepository = accountsRepository;
    }

    public Optional<Account> findById(AccountId accountId) {
        return accountsRepository.findById(accountId);
    }

    @Override
    public Account addNewAccount(Money balance) {
        Validate.notNull(balance);

        Account account = new Account(new AccountId(UUID.randomUUID()), balance);
        accountsRepository.add(account);

        return account;
    }

    @Override
    public Collection<Account> getAll() {
        return accountsRepository.getAll();
    }
}
