package org.ack.domain.accounts.business.api;

import org.apache.commons.lang3.Validate;

public class Transfer {
    private final AccountId sourceAccountId;
    private final AccountId destinationAccountId;
    private final Money amountToBeTransferred;

    public Transfer(AccountId sourceAccountId, AccountId destinationAccountId, Money amountToBeTransferred) {
        Validate.notNull(sourceAccountId);
        Validate.notNull(destinationAccountId);
        Validate.notNull(amountToBeTransferred);

        this.sourceAccountId = sourceAccountId;
        this.destinationAccountId = destinationAccountId;
        this.amountToBeTransferred = amountToBeTransferred;
    }

    public AccountId getSourceAccountId() {
        return sourceAccountId;
    }

    public AccountId getDestinationAccountId() {
        return destinationAccountId;
    }

    public Money getAmountToBeTransferred() {
        return amountToBeTransferred;
    }
}
