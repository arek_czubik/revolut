# Explanation of most important choices

Because of framework constraints I've used HttpServer provided with SDK for REST functionalities.
Since I wanted to have basic usable REST, with most important behaviors, implementation of REST infrastructure grew a bit.
Since it was already big enough I have focused on REST part with domain being as simple as possible.

For rest I wanted:
- to be compliant with Richardson Maturity Model up to level 2, HATEOAS was to much for this simple task
- to have good uri names : plural nouns 
- to use well known response statuses (200, 201, 400, 404, 406, 415)
- to have resources api versioned
- to have support for Content-Type, Accept headers


